<?php


namespace App;


use App\Database\DBRegistry;
use PDO;

class DataLoader
{
    /**
     * @var string
     */
    const BLOB_STORAGE_SQL_QUERY = 'SELECT bs.BlobStorageID, bs.NumReferences as nbRef FROM BlobStorage bs';

    /**
     * @var string
     */
    const REL_SQL_QUERY = 'SELECT %s, %s FROM %s'; // primary, fields, tableName

    /**
     * @var string
     */
    const REL_SQL_ALL_IN_QUERY = 'SELECT "%s" as tableName, %s, %s FROM %s'; // tableName, primary, fields, tableName

    /**
     * @var DBRegistry
     */
    private $registry;

    /**
     * DataLoader constructor.
     */
    public function __construct()
    {
        $this->registry = DBRegistry::getInstance();
    }

    /**
     * blobId => NumRef
     * @return array
     */
    public function loadAllBlobStorage(): array
    {
        return $this->registry->getConnection('global')
            ->query(self::BLOB_STORAGE_SQL_QUERY)
            ->fetchAll(PDO::FETCH_KEY_PAIR);
    }

    /**
     * @param string $tableName
     * @param array $schema
     * @param bool $allInMode
     * @return array
     */
    public function loadOneRelation(string $tableName, array $schema, bool $allInMode = false): array
    {
        $primaryName = $schema['primary'] ?? $tableName . DataConfig::DEFAULT_ID_SUFFIX;
        $fields = implode($schema['fields'], ', ');

        $sql = ($allInMode)
            ? sprintf(self::REL_SQL_ALL_IN_QUERY, $tableName, $primaryName, $fields, $tableName)
            : sprintf(self::REL_SQL_QUERY, $primaryName, $fields, $tableName);

        return $this->registry
            ->getConnection($schema['connection'])
            ->query($sql)->fetchAll();
    }

    /**
     * @param bool $allIn
     * @return array
     */
    public function loadAllRelations(bool $allIn = false): array
    {
        $schemas = [];

        foreach (DataConfig::REL_TABLES_SCHEMA as $tableName => $schema) {
            $data = $this->loadOneRelation($tableName, $schema, $allIn);

            if ($allIn) {
                $schemas[] = $data;
            } else {
                $schemas[$tableName] = $data;
            }
        }

        return ($allIn) ? array_reduce($schemas, 'array_merge', []) : $schemas;
    }
}
