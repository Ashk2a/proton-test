<?php


namespace App\Database;


use PDO;
use PDOStatement;

class DBConnection
{
    /**
     * @var string
     */
    const DSN = "mysql:host=%s;dbname=%s;port=%s;charset=utf8mb4"; // host, db, port

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * DBConnection constructor.
     * @param array $connection
     */
    public function __construct(array $connection)
    {
        $options = [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        ];

        $dsn = sprintf(
            self::DSN,
            $connection['host'],
            $connection['db_name'],
            $connection['port']
        );

        try {
            $this->pdo = new PDO($dsn, $connection['user'], $connection['password'], $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    /**
     * @param $sql
     * @param null $args
     * @return PDOStatement
     */
    public function query($sql, $args = null)
    {
        if (!$args) {
            return $this->pdo->query($sql);
        }

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($args);
        return $stmt;
    }

    /**
     * @return PDO
     */
    public function getPdo(): PDO
    {
        return $this->pdo;
    }
}
