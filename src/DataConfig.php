<?php


namespace App;


class DataConfig
{
    /**
     * @var string
     */
    const DEFAULT_FOREIGN_KEY = 'BlobStorageID';

    /**
     * @var string
     */
    const DEFAULT_ID_SUFFIX = 'ID';

    /**
     * @var array
     */
    const REL_TABLES_SCHEMA = [
        'Attachment' => [
            'fields' => [self::DEFAULT_FOREIGN_KEY],
            'connection' => 'shard'
        ],
        'ContactData' => [
            'fields' => [self::DEFAULT_FOREIGN_KEY],
            'connection' => 'shard'
        ],
        'MessageData' => [
            'primary' => 'MessageID',
            'fields' => ['Body', 'Header'],
            'connection' => 'shard'
        ],
        'OutsideAttachment' => [
            'primary' => 'AttachmentID',
            'fields' => [self::DEFAULT_FOREIGN_KEY],
            'connection' => 'shard'
        ],
        'SentMessage' => [
            'fields' => [self::DEFAULT_FOREIGN_KEY],
            'connection' => 'global'
        ],
        'SentAttachment' => [
            'fields' => [self::DEFAULT_FOREIGN_KEY],
            'connection' => 'global'
        ]
    ];
}
