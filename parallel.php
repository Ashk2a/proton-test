<?php

require __DIR__ . '/vendor/autoload.php';

use App\Database\DBRegistry;
use App\DataConfig;
use App\DataLoader;

if (! function_exists('pcntl_fork')) die('PCNTL functions not available on this PHP installation');

$timeStart = microtime(true);

const NB_THREADS = 4;
const TMP = __DIR__ . '/.tmp.file.';

function getKey($id, $fsize, $flag = 'c')
{
    $path = TMP . $id;

    if (!file_exists($path)) {
        touch($path);
    }

    $shmkey = @shmop_open(ftok($path, 't'), $flag, ($fsize !== 0) ? 0644 : 0, $fsize);

    if (!$shmkey) {
        return false;
    } else {
        return $shmkey;
    }
}

function threadRelationTreatment(array $chunk, array $bsExpectedNumRef): array {
    $results = [
        'count' => [],
        'notExists' => [],
    ];

    foreach ($chunk as $record) {
        $tableName = $record['tableName'];
        $primaryName = DataConfig::REL_TABLES_SCHEMA[$tableName]['primary'] ?? $tableName . DataConfig::DEFAULT_ID_SUFFIX;
        $primaryValue = $record[$primaryName];
        $fields = DataConfig::REL_TABLES_SCHEMA[$tableName]['fields'];

        foreach ($fields as $field) {
            $blobStorageId = $record[$field];

            if (!array_key_exists($blobStorageId, $bsExpectedNumRef)) {
                $results['notExists'][$blobStorageId][] = [$tableName . '.' . $primaryName => $primaryValue];
                continue;
            }

            $results['count'][] = $blobStorageId;
        }
    }

    return $results;
}

$registry = DBRegistry::getInstance();
$loader = new DataLoader();

$bsExpectedNumRef = $loader->loadAllBlobStorage();
$bsActualNumRef = [];
$bsRelations = $loader->loadAllRelations(true);

$results = [
    'zeroCount' => [],
    'diffCount' => [],
    'notExists' => [],
];

$chunks = array_chunk($bsRelations, ceil(count($bsRelations) / NB_THREADS));

foreach ($chunks as $k => $chunk) {
    $pid = pcntl_fork();
    $id = $k + 1;

    if ($pid == -1) {
        exit("Error forking...\n");
    } else if ($pid == 0) {
        $serializeData = serialize(threadRelationTreatment($chunk, $bsExpectedNumRef));
        $fsize = mb_strlen($serializeData, '8bit');

        $childKey = getKey($id, $fsize);
        shmop_write($childKey, $serializeData, 0);
        shmop_close($childKey);
        exit();
    }
}

while (pcntl_waitpid(0, $status) != -1);

for ($i = 1; $i <= NB_THREADS; $i++) {
    $childKey = getKey($i, 0, 'a');
    $content = unserialize(shmop_read($childKey, 0, 0));

    $bsActualNumRef = array_merge($bsActualNumRef, $content['count']);

    // Didn't find a native function to do this merge and conserve keys...
    foreach ($content['notExists'] as $blobStorageId => $row) {
        if (array_key_exists($blobStorageId, $results['notExists'])) {
            $results['notExists'][$blobStorageId] = array_merge($results['notExists'][$blobStorageId], $row);
        } else {
            $results['notExists'][$blobStorageId] = $row;
        }
    }

    shmop_delete($childKey);
    unlink(TMP . $i);
}

$bsActualNumRef = array_count_values($bsActualNumRef);

foreach ($bsActualNumRef as $blobStorageId => $actualCount) {
    $expectedCount = $bsExpectedNumRef[$blobStorageId];

    if ($actualCount === 0 && $expectedCount === 0) {
        $results['zeroCount'][] = $blobStorageId;
    } elseif ($actualCount !== $expectedCount) {
        $results['diffCount'][$blobStorageId] = [
            'expected' => $expectedCount,
            'actual' => $actualCount,
            'toDelete' => $actualCount === 0
        ];
    }

    unset($bsExpectedNumRef[$blobStorageId]);
}

$results['zeroCount'] = array_merge($results['zeroCount'], array_keys($bsExpectedNumRef));

$fp = fopen('results_multi_threads.json', 'w');
fwrite($fp, json_encode($results, JSON_PRETTY_PRINT));
fclose($fp);

echo 'Execution finish, during ' . (microtime(true) - $timeStart) . ' seconds' . PHP_EOL;
